using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using WebApplication14.Models;

namespace WebApplication14.Pages
{
    public class RegistrationModel : PageModel
    {
        [BindProperty]
		public Registration Registration { get; set; }

        public void OnGet()
        {
        }

        public IActionResult OnPost() {
            Console.WriteLine(Registration.Subject);
            if(Registration.Subject=="������"&& Registration.Date.DayOfWeek == DayOfWeek.Monday)
            {
				ModelState.AddModelError("Registration.Subject", "������ �� ������ ��������� �� �����i����");
			}
			if (!ModelState.IsValid)
			{
				return Page();
			}
			return RedirectToPage('/');

		}

    }
}
