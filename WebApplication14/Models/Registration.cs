﻿using System.ComponentModel.DataAnnotations;
using WebApplication14.Validation;

namespace WebApplication14.Models
{
	public class Registration
	{
		[Required(ErrorMessage = "Не вказано iм'я")]
		[Display(Name = "Iм'я та прізвище")]
		public string FullName { get; set; }
		[Required(ErrorMessage = "Не вказано пошту")]
		[EmailAddress(ErrorMessage = "Некоректна адреса")]
		[Display(Name = "Електронна пошта")]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }
		[Required(ErrorMessage = "Не вказано дату")]
		[DateValidation(ErrorMessage ="Не може бути в вихiдний чи менше сьогоднiшньоi дати")]
		[Display(Name = "Дата запису")]
		[DataType(DataType.DateTime)]
		public DateTime Date {  get; set; }
		[Required(ErrorMessage = "Не вказано предмет")]
		[Display(Name = "Предмет")]
		[SubjectValidation(new string[] { "JavaScript", "C#", "Java", "Python", "Основи" }, ErrorMessage = "Неприпустиме значення")]
		public string Subject { get; set; }
	}
}
