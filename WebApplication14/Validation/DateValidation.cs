﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace WebApplication14.Validation
{
	public class DateValidationAttribute: ValidationAttribute
	{
		public override bool IsValid(object? value)
		{
			if (value == null)
				return false;

			if (value is DateTime valueDate)
			{
				DateTime now = DateTime.Now;
				return now.Date < valueDate.Date && valueDate.DayOfWeek != DayOfWeek.Sunday && valueDate.DayOfWeek != DayOfWeek.Saturday;
			}

			return false;
		}
	}
}
