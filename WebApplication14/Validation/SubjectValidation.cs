﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace WebApplication14.Validation
{
	public class SubjectValidationAttribute : ValidationAttribute
	{
		string[] _subjects;
		public SubjectValidationAttribute(string[] subjects)
		{
			_subjects = subjects;
		}

		public override bool IsValid(object? value)
		{
			return value != null && _subjects.Contains(value);
		}
	}
}
